package ru.rt;

public class Kosaraiu {
    int [][] graph;//graph
    int [][] invertedGraph;//graph
    SimpleQueue queue;
    SingleArray<Integer> used;
    SimpleQueue components;
    int[] result;
    int number = 0;

    public Kosaraiu(int [][] graph){
        this.graph = graph;
        this.invertedGraph = createInvertGraph();
        this.used = new SingleArray();
        this.components = new SimpleQueue();
        this.queue = new SimpleQueue();
        this.result = new int[graph.length];
    }

    public int[] calc(){
        for (int i = 0; i < invertedGraph.length; i++) {
            dfs(invertedGraph, i);
        }
        this.queue = this.components;
        this.components = new SimpleQueue();
        this.used = new SingleArray();
        while (!queue.empty()){
            Integer dequeue = queue.dequeue();
            dfs(graph, dequeue);
            if(!components.empty())  {
                calcResult();
            }
            components = new SimpleQueue();
        }
        return result;
    }

    private void calcResult() {
        while (!components.empty()){
            result[components.dequeue()] = number;
        }
        number++;
    }

    private void dfs(int [][] graph, int v){
        if(used.contains(v)) return;
        used.put(v);
        for (int i = 0; i < graph.length; i++) {
            int elem = graph[v][i];
            if(elem == 1){
                dfs(graph, i);
            }
        }
        components.enqueue(v);
    }

    private int [][] createInvertGraph() {
        int length = graph.length;
        int [][] invertedGraph = new int[length][length];
        for (int i = 0; i < length; i++) {
            for (int j = 0; j < length; j++) {
                invertedGraph[j][i] = graph[i][j];
            }
        }
        return invertedGraph;
    }

}
