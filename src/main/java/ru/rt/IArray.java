package ru.rt;

public interface IArray<T> {
    int size();
    boolean isEmpty();
    T get(int index);
    void put(T item);
    void put(T item, int index);
    T remove(int index);
}
