package ru.rt;

public class SimpleQueue {
    Integer value;
    SimpleQueue next;

    SimpleQueue(){
        this.value = null;
        next = null;
    }

    SimpleQueue(Integer value){
        this.value = value;
        next = null;
    }

    public void enqueue(Integer value){
        if(empty()){
            this.value = value;
        }else if(next == null){
            next = new SimpleQueue(value);
        }else {
            next.enqueue(value);
        }

    }

    public Integer dequeue(){
        if(next == null){
            Integer value = this.value;
            this.value = null;
            return value;
        }
        if(next.next == null){
            int value = next.value;
            this.next = null;
            return value;
        }
        return next.dequeue();
    }

    public boolean empty() {
        return this.next == null && this.value == null;
    }

    @Override
    public String toString() {
        return "SimpleQueue{" +
                "value=" + value +
                ", next=" + next +
                '}';
    }
}
